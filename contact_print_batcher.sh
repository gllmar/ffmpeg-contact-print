#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

BATCH_FOLDER="$1"
shopt -s nullglob # Sets nullglob
shopt -s nocaseglob # Sets nocaseglob
for movie in "$BATCH_FOLDER"/*.{mov,mp4,m4v}; do
    echo "--------------------------- PROCESSING $movie -----------------------------"  
    "$SCRIPTPATH"/contact_print_maker.sh "$movie" "$2"
done
