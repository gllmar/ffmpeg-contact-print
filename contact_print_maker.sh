#!/bin/bash
# input : ffmpeg compatible movie file
# output : PDF page with 36 timecoded frames (6x6 view) + meta data
# Dependencies : 
#   ffmpeg (brew install ffmpeg)
#   imagemagick (brew install imagemagick)
#   pdflatex (brew cask install basictex )
#        sudo tlmgr update --self && sudo tlmgr install fancyhdr   
#        sudo tlmgr update --self && sudo tlmgr install lastpage  

void_usage()
{
    cat <<EOT 
    contact_print_maker.sh 
    \$1 => movie files
    \$2 =>  pages count (opt)
EOT
    exit
}

void_setup()
{
    if [ $# -eq 0 ] 
    then
        echo "Feed me a movie file path"
        read FILEPATH
    else
        FILEPATH="${1}"
    fi    

    echo "=========== setup ==========="
    if [ "$2" == "" ] 
    then 
        NUM_PAGES=1
    else 
        NUM_PAGES=$2
    fi
    echo $NUM_PAGES


    THUMBSCOUNT=$(($NUM_PAGES*36))
    DIR=$(dirname "${FILEPATH}")
    FILE=$(basename "${FILEPATH}")
    NAME="${FILEPATH##*/}"
    NAME="${NAME%.*}"
    
    mkdir -p "$DIR/$NAME"

}

void_get_info()
{
    echo "=========== get_info ==========="
    echo $NAME
    TOTALFRAMES=`ffprobe -v error -select_streams v:0 -show_entries stream=nb_frames -of default=nokey=1:noprint_wrappers=1 "${FILEPATH}"`
    TOTALTIME=`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 -sexagesimal "${FILEPATH}"`
    FRAMERATE=`ffprobe -v error -select_streams v:0 -show_entries stream=avg_frame_rate -of default=noprint_wrappers=1:nokey=1 "${FILEPATH}"`
}

## extract thumbnails frames and redirect log to txt 
void_extract_thumbs()
{
    echo "=========== extract_thumbs ==========="
    ffmpeg  -i "${FILEPATH}"  -vframes $THUMBSCOUNT -vf "thumbnail=$((TOTALFRAMES / THUMBSCOUNT)),
        scale=640:480:force_original_aspect_ratio=decrease,pad=640:480:(ow-iw)/2:(oh-ih)/3:color=white, 
        drawtext=fontsize=48:fontcolor=black:x=(W-tw)/2:y=H-th-10:
        shadowcolor=white:shadowx=2:shadowy=2:
        text='%{pts\:hms}' " -vsync vfr $DIR/$NAME/out%04d.png 
}

## create column with frame number
void_thumbs_to_grid()
{
    echo "=========== thumbs_to_grid ==========="
    montage "$DIR/$NAME/"*.png -geometry +2+2  -tile 6x6  -background white "$DIR/$NAME/$NAME.jpg"
}

## create PDF with pictures and fill in metadata
void_create_pdf()
{
    echo "=========== create_pdf ==========="

    rm "$DIR/$NAME/build.tex"
    cat <<EOT >> "$DIR/$NAME/build.tex"
        \documentclass[hidelinks]{article}
        \usepackage{fancyhdr}
        \usepackage[margin=2cm, papersize={11in, 8.5in}]{geometry}
        \usepackage{eso-pic,graphicx}
        \usepackage[T1]{fontenc}
        \usepackage[pdftex,
            pdfauthor={`whoami`},
            pdftitle={$FILE},
            pdfsubject={},
            pdfkeywords={RUNTIME=$TOTALTIME, Frames=$TOTALFRAMES, FPS:$FRAMERATE},
            pdfproducer={contact print maker},
            pdfcreator={pdflatex}]{hyperref}
        \usepackage{lastpage}
        \usepackage[strings]{underscore}
        \renewcommand{\familydefault}{\ttdefault} 
        \pagestyle{fancy}
        \lhead{$FILE}
        \rhead{$TOTALFRAMES@$FRAMERATE FPS - $TOTALTIME}
        \lfoot{$DIR}
        \cfoot{}
        \rfoot{\thepage/\pageref{LastPage}}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
        \begin{document}
EOT

## create as much page as they are jpgs in the folder
    for image in "$DIR/$NAME/"*.jpg; do
        [ -f "$image" ] || break
        echo "\hfill\includegraphics[width=0.83\paperwidth,height=0.83\paperheight,keepaspectratio]{$image}" >> "$DIR/$NAME/build.tex"
        echo "\newpage" >> "$DIR/$NAME/build.tex"
    done

    echo "\end{document}" >> $DIR/$NAME/build.tex

# needed to be run twice to get last page of the output
    pdflatex -output-directory "$DIR/$NAME/"  "$DIR/$NAME/build.tex" 
    pdflatex -output-directory "$DIR/$NAME/"  "$DIR/$NAME/build.tex" 
    mv "$DIR/$NAME/build.pdf" "$DIR/$NAME.pdf"
}

void_clean_build_folder()
{
    rm "$DIR/$NAME/"build.*
    rm "$DIR/$NAME/"*.png
    rm "$DIR/$NAME/"*.jpg
    rmdir "$DIR/$NAME"
}

## main()
void_setup "${@}" 
void_get_info
void_extract_thumbs
void_thumbs_to_grid
void_create_pdf
void_clean_build_folder
