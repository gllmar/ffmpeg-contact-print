# Contact-print-maker.sh

## Usage 

### contact-print-maker.sh

#### input

$1 : ffmpeg compatible movie file
$2 : number of pages (optional)

#### output 

PDF page with 36 timecoded frames (6x6 view) + meta data

### contact_print_batcher.sh

#### input
$1 : folder with ffmpeg compatible movie file
$2 : number of pages (optional)

#### output 

X PDF files in a folder with with 36 timecoded frames (6x6 view) + meta data


## Dependencies

* ffmpeg 
	* `brew install ffmpeg`
* imagemagick 
	* `brew install imagemagick`
* pdflatex 
	* `brew cask install basictex `
* fancyhdr
	* `sudo tlmgr update --self && sudo tlmgr install fancyhdr`   
* lastpage
	* `sudo tlmgr update --self && sudo tlmgr install lastpage`  

## 